<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Movie;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('pages.index',compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'movieName' => 'required',
            'movieDescription' => 'required',
            'movieReleaseDate' => 'required',
            'movieDuration' => 'required'
        ));

        $post = new Movie;

        $post->movieName = $request->movieName;
        $post->movieDescription = $request->movieDescription;
        $post->movieReleaseDate = $request->movieReleaseDate;
        $post->movieDuration = $request->movieDuration;

        $post->save();

        return redirect()->route('pages.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Movie::find($id);
        return view('pages.show',compact('Movie'))->withMovie($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movie = Movie::find($id);
        return view('pages.edit',compact('Movie'))->withMovie($movie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'movieName' => 'required',
            'movieDescription' => 'required',
            'movieReleaseDate' => 'required',
            'movieDuration' => 'required'
        ));

        $movie = Movie::find($id);

        $movie->movieName = $request->input('movieName');
        $movie->movieDescription = $request->input('movieDescription');
        $movie->movieReleaseDate = $request->input('movieReleaseDate');
        $movie->movieDuration = $request->input('movieDuration');

        $movie->save();

        return redirect()->route('pages.show', $movie->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);

        $movie->delete();

        return redirect()->route('pages.index');
    }
}
