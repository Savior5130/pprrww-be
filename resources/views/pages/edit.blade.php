@extends('pages.layout')

@section('content')

<form action="{{ route('pages.update',$movie->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="movieName" value="{{ $movie->movieName }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Detail:</strong>
                    <textarea class="form-control" style="height:150px" name="movieDescription" placeholder="Detail">{{ $movie->movieDescription }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Price:</strong>
                    <input type="text" name="movieReleaseDate" value="{{ $movie->movieReleaseDate }}" class="form-control" placeholder="Price">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Price:</strong>
                    <input type="text" name="movieDuration" value="{{ $movie->movieDuration }}" class="form-control" placeholder="Price">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                 <button class="btn btn-primary">Submit</button><a class="btn btn-primary" href="{{ route('pages.index') }}">Cancel</a>
            </div>
        </div>
    </form>

@endsection