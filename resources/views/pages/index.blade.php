@extends('pages.layout')

@section('content')

<table class="table">
<thead>
    <th>id</th>
    <th>name</th>
    <th>description</th>
    <th>release date</th>
    <th>duration</th>
</thead>
    <tbody>
        @foreach($movies as $movie)
    <tr>
        <th>{{ $movie->id }}</th>
        <td>{{ $movie->movieName }}</td>
        <td>{{ substr($movie->movieDescription, 0 , 50) }}{{ strlen($movie->movieDescription) > 50 ? "..." : "" }}</td>
        <td>{{ $movie->movieReleaseDate }}</td>
        <td>{{ $movie->movieDuration }}</td>
        
        <form action="{{ route('pages.destroy', $movie->id) }}" method="POST">
        <td><a href="{{ route('pages.show', $movie->id) }}" class="btn btn-primary">view</a><a href="{{ route('pages.edit', $movie->id) }}" class="btn btn-primary">edit</a>
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">delete</button></td>
        </form>
    </tr>
        @endforeach
    </tbody>
</table>

<a class="btn btn-primary" href="{{ route('pages.create') }}">create</a>

@endsection