@extends('pages.layout')

@section('content')

<h1>hello</h1>
<hr>
<form action="{{ route('pages.store') }}" method="POST">
@csrf
    <label for="fname">Movie name:</label><br>
    <input type="text" name="movieName" class="form-control" placeholder="MovieName">
    <label for="lname">Description:</label><br>
    <input type="text" name="movieDescription" class="form-control" placeholder="MovieDescription">
    <label for="fname">Release Date:</label><br>
    <input type="text" name="movieReleaseDate" class="form-control" placeholder="MovieReleaseDate">
    <label for="lname">Duration:</label><br>
    <input type="text" name="movieDuration" class="form-control" placeholder="MovieDuration">

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@endsection