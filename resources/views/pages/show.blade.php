@extends('pages.layout')

@section('content')

<p>success</p>

<h1>{{ $movie->movieName }}</h1>
<h1>{{ $movie->movieDescription }}</h1>
<h1>{{ $movie->movieReleaseDate }}</h1>
<h1>{{ $movie->movieDuration }}</h1>
<form action="{{ route('pages.destroy', $movie->id) }}" method="POST">
        <td><a class="btn btn-primary" href="{{ route('pages.index') }}">continue</a><a href="{{ route('pages.edit', $movie->id) }}" class="btn btn-primary">edit</a>
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">delete</button></td>
        </form>
@endsection